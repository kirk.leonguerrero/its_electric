

from time import time, sleep
from queue import Queue

from lte.$metered_service_name.sentinel import Sentinel
from lte.common.notifications import slack


class SchedulerError(Exception):
    pass


class Scheduler(object):
    def __init__(self, config, thread_count=3):
        self.config = config
        self._sentinel_pool = []
        self._request_queue = Queue()
        self.thread_count = thread_count

        for _ in thread_count:
            sentinel = Sentinel(self.config, self._request_queue)
            sentinel.start()
            _sentinel_pool.append(sentinel)

    def is_alive(self):
        for sentinel in self._sentinel_pool:
            if not sentinel.is_alive():
                return False

        return True

    @property
    def assets(self):
        return [asset for asset in self.config['assets']]

    def _schedule(self, asset_id):
        task_id = hash(time())
        job = {'task_id': task_id, 'asset_id': asset_id}

        try:
            self._request_queue.put(job, block=True, timeout=1.0)
        except QueueTimeout:
            slack('dc-alerts', 'Queue timeout in scheduler')
            # Prevent DOS'ing our ourselves, wait before we resume operation
            sleep(60.0)
            return

        return task_id

    def schedule(self, asset_id):
        return self._schedule(asset_id)

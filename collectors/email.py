

import requests
from lte.$metered_service_name.collectors import register_collector
from sendgrid import SendGrid


@register_collector('email')
class Email(object):
    def __init__(self):
        self.provider = SendGridLibrary()

    def _get(self, params):
        return self.provider.get(params)

    def get(self, params):
        return self._get(params)

    def _write(self, data):
        some_db.insert(data)

    def write(self, data):
        self._write(data)



collectors = {}


def register_collector(provider):
    def wrap_collector(cls):
        """Wraps a data provider class"""
        if provider not in collectors:
            collectors[provider] = cls

    return wrap_collector

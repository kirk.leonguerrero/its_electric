

import requests
from lte.$metered_service_name.collectors import register_collector


@register_collector('power_factors')
class PowerFactors(object):
    def _get(self, params):
        return requests.get(self.url)

    def get(self, params):
        return self._get(params)

    def _write(self, data):
        some_db.insert(data)

    def write(self, data):
        self._write(data)

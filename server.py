

from lte.common.web import web
from lte.$metered_service_name import Scheduler


scheduler = Scheduler(some_config)


@web.get('/v1/collect/<asset_id:int>')
def collect(asset_id):
    try:
        task_id = scheduler.schedule(asset_id)
        return 200, {'status': 'OK', 'task_id': task_id}
    except SomeSchedulerError as e:
        return 500, {'status': 'NOK', 'message': e}

@web.get('/v1/collect')
def collect_all():
    task_ids = []
    task_failures = []

    for asset_id in scheduler.assets:
        try:
            task_ids.append(scheduler.schedule(asset_id))
        except SomeSchedulerError as e:
            task_failures.append({'asset_id': asset_id, 'error': e})

    if task_failures:
        return 500, {'status': 'NOK', 'errors': task_failures, 'tasks_successful': task_ids}

    return 200, {'status': 'OK', 'task_ids': task_ids}

@web.get
def health():
    if scheduler.is_alive():
        return {'status': 'OK'}
    else:
        return {'status': 'NOK'}

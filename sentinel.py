


from threading import Thread

from lte.$metered_service_name.collectors import collectors
from lte.common.notifications import slack


class Sentinel(Thread):
    def __init__(self, config, request_queue):
        super().__init__()
        self.config = config
        self.request_queue = request_queue

    def run(self):
        while True:
            job = self.request_queue.get()
            asset_id = job.get('asset_id')
            task_id = job.get('task_id')
            provider = get_provider_by_asset_id(asset_id)

            try:
                collector = collectors[provider]
            except KeyError:
                slack('dc-alerts', 'Provider {} is not a valid provider for asset {}'.format(provider, asset_id))
                continue

            asset_request_params = get_asset_request_params(asset_id)
            data_provider_params = get_data_provider_params(provider)

            request_params = {**asset_request_params, **data_provider_params}

            data = collector.get(request_params)
            collector.write(data)
